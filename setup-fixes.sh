#!/bin/bash

#
#   #############
#   PUBLIC DOMAIN
#   #############
#
#   To the extent possible under law, Oto Šťáva has waived all copyright and
#   related or neighboring rights to this script.
#   Published from: Czech Republic
#



# Root user check
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root!" 
   exit 1
fi


# Copy brcm firmware
echo "--- Fixing Broadcom SDIO WiFi ---"
cp -f firmware/brcm/* /lib/firmware/brcm/


# Blacklist HDMI audio
echo "--- Fixing audio ---"
echo "blacklist snd-hdmi-lpe-audio" > /etc/modprobe.d/blacklist-acer-hdmi.conf


# Defer i915
echo "--- Fixing screen brightness ---"
echo "blacklist i915" > /etc/modprobe.d/blacklist-acer-i915.conf
mkdir /etc/i915-brightness-fix
cp brightness/i915-start.sh /etc/i915-brightness-fix/i915-start.sh
chmod +x /etc/i915-brightness-fix/i915-start.sh
cp brightness/i915-brightness-fix.service /etc/systemd/system/i915-brightness-fix.service
systemctl enable i915-brightness-fix


# Copy intel firmware
echo "--- Fixing i915 firmware errors ---"
cp -f firmware/i915/* /lib/firmware/i915/

echo "All done, please regenerate your initramfs and reboot."
